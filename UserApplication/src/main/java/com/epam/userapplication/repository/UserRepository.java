package com.epam.userapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.userapplication.data.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}

package com.epam.userapplication.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.epam.userapplication.exception.UserException;
import com.epam.userapplication.model.UserDTO;
import com.epam.userapplication.service.UserService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@RestController
public class UserRestController {
	@Autowired
	private UserService userService;

	@GetMapping("/users")
//	@HystrixCommand(fallbackMethod = "fallbackOvertime", commandProperties = {
//			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000") })
	public ResponseEntity<List<UserDTO>> getAllUsers() {
		return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
	}

	@GetMapping("/users/{userId}")
	public ResponseEntity<UserDTO> getUserById(@PathVariable int userId) throws UserException {
		return new ResponseEntity<UserDTO>(userService.getUserById(userId), HttpStatus.OK);
	}

	@PostMapping("/users")
	public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO userDTO) throws UserException {
		return new ResponseEntity<UserDTO>(userService.addUser(userDTO.getUserId(), userDTO.getUsername(), userDTO.getPassword()),
				HttpStatus.OK);
	}

	@DeleteMapping("/users/{userId}")
	public ResponseEntity<Boolean> deleteUser(@PathVariable int userId) throws UserException {
		return new ResponseEntity<Boolean>(userService.deleteUser(userId), HttpStatus.OK);
	}

	@PutMapping("/users/{userId}")
	public ResponseEntity<UserDTO> updateUser(@PathVariable int userId, @RequestBody UserDTO userDTO)
			throws UserException {
		return new ResponseEntity<UserDTO>(userService.updateBook(userId, userDTO.getUsername(), userDTO.getPassword()),
				HttpStatus.OK);
	}

}

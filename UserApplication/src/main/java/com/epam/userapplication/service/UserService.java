package com.epam.userapplication.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.userapplication.data.User;
import com.epam.userapplication.exception.UserException;
import com.epam.userapplication.model.UserDTO;
import com.epam.userapplication.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ModelMapper modelmapper;

	User user;
	
	public List<UserDTO> getAllUsers() {
		return modelmapper.map(userRepository.findAll(), new TypeToken<List<UserDTO>>() {
		}.getType());
	}

	public UserDTO addUser(int userId, String username, String password) throws UserException{
		if(!(userRepository.findById(userId).isEmpty())) {
			throw new UserException("User with specified Id already exists");
		}
		if(username.isEmpty()) {
			throw new UserException("Invalid Username");
		}
		if(password.isEmpty()) {
			throw new UserException("Invalid Password");
		}
		user = new User();
		user.setUsername(username);
		user.setPassword(password);
		return modelmapper.map(userRepository.save(user), UserDTO.class);
	}

	public UserDTO getUserById(int userId) throws UserException{
		if(userRepository.findById(userId).isEmpty()) {
			throw new UserException("Invalid UserId");
		}
		return modelmapper.map(userRepository.getById(userId), UserDTO.class);
	}

	public UserDTO updateBook(int userId, String username, String password) throws UserException {
		if(userRepository.findById(userId).isEmpty()) {
			throw new UserException("Invalid User Id");
		}
		if(username.isEmpty()) {
			throw new UserException("Invalid Username");
		}
		if(password.isEmpty()) {
			throw new UserException("Invalid Password");
		}
		user = userRepository.getById(userId);
		user.setUsername(username);
		user.setPassword(password);
		return modelmapper.map(userRepository.save(user), UserDTO.class);
	}

	public boolean deleteUser(int userId) throws UserException{
		if(userRepository.findById(userId).isEmpty()) {
			throw new UserException("Invalid User Id");
		}
		userRepository.deleteById(userId);
		return !userRepository.findById(userId).isPresent();
	}

}
